import Joi from 'joi'
import userSchema, {idSchema,userUpdateSchema} from '../validators/user.schemas.mjs'

export const validatePaginationMW = async (req,res,next) => {
    const paginationSchema = Joi.object({
        pageNumber: Joi.number().min(0),
        itemsPerPage: Joi.number().max(10)
    })
    req.query = await paginationSchema.validateAsync(req.query)
    next()
}

export const validateUserInsertMW = async (req,res,next) => {
    try{
        const value = await userSchema.validateAsync({first_name:req.body.first_name,
            last_name:req.body.last_name,
            email:req.body.email,
            phone:req.body.phone
          });
        req.body = value
        next()
    }catch(err){
        next(err)
    }
}

export const validateUserUpdateMW = async (req,res,next) => {
    try{
        const value = await userUpdateSchema.validateAsync({first_name:req.body.first_name,
            last_name:req.body.last_name,
            email:req.body.email,
            phone:req.body.phone
          });
        req.body = value
        next()
    }catch(err){
        next(err)
    }
}

export const validateUserIdMW = async (req,res,next) => {
    try{
        const idObj = await idSchema.validateAsync({id:req.params.id})
        req.params.id = idObj.id
        next()
    }catch(err){
        next(err)
    }
}