import Joi from 'joi'

const userSchema = Joi.object({
    first_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

        last_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    phone: Joi.string().length(10).pattern(/^[0-9]+$/).required(),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).required()
})

export const userUpdateSchema = Joi.object({
    first_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30),

        last_name: Joi.string()
        .alphanum()
        .min(3)
        .max(30),

    phone: Joi.string().length(10).pattern(/^[0-9]+$/),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
})

export const idSchema = Joi.object({
    id: Joi.string()
        .alphanum()
        .min(24)
        .max(24)
})

export default userSchema