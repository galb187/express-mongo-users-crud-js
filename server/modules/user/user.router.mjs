/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import wrapper from "../../middleware/route.async.wrapper.mjs";
import user_model from "./user.model.mjs";
import express from 'express';
import log from '@ajar/marker';
import {validatePaginationMW, validateUserIdMW,validateUserInsertMW,
  validateUserUpdateMW
} from '../../middleware/validation.mjs'

const router = express.Router();

// parse json req.body on post routes
router.use(express.json())

// get paginated data
router.get('/paginate', wrapper(validatePaginationMW)
                      , async(req,res)=>{
  const {pageNumber = 0, itemsPerPage = 10} = req.query
  const paginatedData = await user_model.find()
                                    .skip((pageNumber-1)*itemsPerPage)
                                    .limit(itemsPerPage)
  res.status(200).json(paginatedData);
})

// GETS A SINGLE USER
router.get("/:id",wrapper(validateUserIdMW),wrapper(async (req, res) => {
  log.blue(req.params.id)
    const user = await user_model.findById(req.params.id)
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
  })
);


// GET ALL USERS
router.get( "/",wrapper(async (req, res) => {
    const users = await user_model.find()
                                  .select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);
    res.status(200).json(users);
  })  
);

// CREATES A NEW USER
router.post("/",wrapper(validateUserInsertMW),
                 wrapper( async (req, res) => {
    const user = await user_model.create(req.body);
    res.status(200).json(user);
}) );


// UPDATES A SINGLE USER
router.put("/:id",wrapper(validateUserIdMW)
                  ,wrapper(validateUserUpdateMW)
                  ,wrapper(async (req, res) => {
    const user = await user_model.findByIdAndUpdate(req.params.id,req.body, 
                                                    {new: true, upsert: false });
    res.status(200).json(user);
  })
);


// DELETES A USER
router.delete("/:id",wrapper(validateUserIdMW)
                    ,wrapper(async (req, res) => {
    const user = await user_model.findByIdAndRemove(req.params.id);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
  })
);

export default router;
